const db = require("../models/index.js");
const TasksSegment = db.taskssegment;

// Create and Save a new TasksSegment
exports.create = (req, res) => {
// Validate request
  if (!req.body.title) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }
  
    const tsegment = new TasksSegment({
        title: req.body.title,
        description: req.body.description,
        // id: ,
        tasks: req.body.tasks})

    tsegment.save(tsegment).then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send(
            {
                message:
                err.message || "Some error occured while creating the TasksSegment"
            }
        )
    })
};

// Retrieve all TasksSegments from the database.
exports.findAll = (req, res) => {
  const title = req.query.title;
  var condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};

  TasksSegment.find(condition)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving TaskSegments."
      });
    });
};

// Find a single TasksSegment with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  TasksSegment.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found TasksSegment with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving TasksSegment with id=" + id });
    });
};

// Update a TasksSegment by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const id = req.params.id;

  TasksSegment.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update TasksSegment with id=${id}. Maybe TasksSegment was not found!`
        });
      } else res.send({ message: "TasksSegment was updated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating TasksSegment with id=" + id
      });
    });
};

// Delete a TasksSegment with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  TasksSegment.findByIdAndRemove(id)
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete TasksSegment with id=${id}. Maybe TasksSegment was not found!`
        });
      } else {
        res.send({
          message: "TasksSegment was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete TasksSegment with id=" + id
      });
    });
};

// Delete all TasksSegments from the database. TODO: should require admin privileges
exports.adminDeleteAll = (req, res) => {
  TasksSegment.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} TasksSegments were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all TasksSegments."
      });
    });
};



// Find all published TasksSegments
exports.findAllPublished = (req, res) => {
  TasksSegment.find({ published: true })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving TasksSegments."
      });
    });
};