const db = require("../models/index.js");
const Project = db.project;
const User = db.user; // needed for User.assignToProject

// Create and Save a new User
exports.create = (req, res) => {
// Validate request
  if (!req.body.username) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }
  
    const project = new Project({
        title: req.body.title,
        description: req.body.description,
        // id: ,
      })

    Project.save(project).then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send(
            {
                message:
                err.message || "Some error occured while creating the User."
            }
        )
    })
};

// Retrieve all Users from the database.
exports.findAll = (req, res) => {
  const title = req.query.title;
  var condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};

  Project.find(condition)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Users."
      });
    });
};

exports.assignToProject = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const userID = req.params.userID;
  const projectID = req.params.projectID;

  // first try to find project -- safeguard against project to assign not existing
  Project.findById(projectID).catch(err => res.status(500).send({message: "Could not find Project with id="+projectID})); // sa
  let projectRef = new DBRef('Project',new ObjectId(projectId))
  
  // try to find user -- we need to get original array of isInProjects to extendit
  User.findById(userID).then(
    data => {
      let projectsUserIsIn = data.isInProjects;
      projectsUserIsIn.push(projectRef);
      User.findByIdAndUpdate(id, {isInProjects:projectsUserIsIn}, { useFindAndModify: false })
      .then(data => {
        if (!data) {
          res.status(404).send({
            message: `Cannot update User with id=${id}. Maybe User was not found!`
          });
        } else res.send({ message: "User was updated successfully." });
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating User with id=" + id
        });
      });
    }
  ).catch(err => {
    res.status(500).send({
      message: "Error updating User with id=" + id
    });
  });
    // TODO: the same for Projects -- two-way bind
};

exports.removeFromProject = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const userID = req.params.userID;
  const projectID = req.params.projectID;

  // first try to find project -- safeguard against project to assign not existing
  Project.findById(projectID).catch(err => res.status(500).send({message: "Could not find Project with id="+projectID})); // sa
  let projectRef = new DBRef('Project',new ObjectId(projectId))
  
  // try to find user -- we need to get original array of isInProjects to extendit
  User.findById(userID).then(
    data => {
      let projectsUserIsIn = data.isInProjects;
      let positionsToRemove = [];
      projectsUserIsIn.forEach((e,i)=>{if(e.$id == projectID){positionsToRemove.push(i)}});
      positionsToRemove.forEach((index)=>projectsUserIsIn.splice(position,1));
      projectsUserIsIn.push(projectRef);
      User.findByIdAndUpdate(id, {isInProjects:projectsUserIsIn}, { useFindAndModify: false })
      .then(data => {
        if (!data) {
          res.status(404).send({
            message: `Cannot update User with id=${id}. Maybe User was not found!`
          });
        } else res.send({ message: "User was updated successfully." });
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating User with id=" + id
        });
      });
    }
  ).catch(err => {
    res.status(500).send({
      message: "Error updating User with id=" + id
    });
  });
  // TODO: the same for Projects -- two-way bind
};

// Find a single User with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  User.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found User with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving User with id=" + id });
    });
};

// Update a User by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const id = req.params.id;

  User.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update User with id=${id}. Maybe User was not found!`
        });
      } else res.send({ message: "User was updated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating User with id=" + id
      });
    });
};

// Delete a User with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  User.findByIdAndRemove(id)
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete User with id=${id}. Maybe User was not found!`
        });
      } else {
        res.send({
          message: "User was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete User with id=" + id
      });
    });
};

// Delete all Users from the database. TODO: should require admin privileges
exports.adminDeleteAll = (req, res) => {
  User.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} Users were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Users."
      });
    });
};



// Find all published Users
exports.findAllPublished = (req, res) => {
  User.find({ published: true })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Users."
      });
    });
};
