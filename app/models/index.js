const databaseConfig = require("../config/db.config.js");

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const database = {
    mongoose: mongoose,
    url: databaseConfig.url,
    taskssegment: require("./tsegment.model.js")(mongoose),
    project: require("./project.model.js")(mongoose)

};

module.exports = database;