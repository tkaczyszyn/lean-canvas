module.exports = mongoose => {
    var schema = mongoose.Schema(
        {
          title: String,
          description: String,
          id: Number,
          tasks: [String]
        },
        { timestamps: true }
      )
      
    schema.method("toJSON", function() { // make id public; relevant for delete-able types
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
    
    const TasksSegment = mongoose.model("taskssegment", schema);
    return TasksSegment;
    };
