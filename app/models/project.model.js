const { SchemaTypes } = require("mongoose");
const db = require("../models/index.js");

module.exports = mongoose => {
  //const TasksSegment = mongoose.model("TasksSegment",schema);
    var schema = mongoose.Schema(
        {
          title: String,
          description: String,
          hasUsers: [{type:SchemaTypes.ObjectId, // TODO implement
            ref:"User"}],
          // usersCurrentlyEditing: [{user:{type:SchemaTypes.ObjectId,
          //  ref:"User"},lastEditTimestamp: Date}], // TODO when simultaneous editing needs to be implemented
          id: Number,
          segments: {
            problem: {type: SchemaTypes.ObjectId,
              ref: "TasksSegment"},
            existingAlternatives: {type: SchemaTypes.ObjectId,
              ref: "TasksSegment"},
            solution: {type: SchemaTypes.ObjectId,
              ref: "TasksSegment"},
            indicators: {type: SchemaTypes.ObjectId,
              ref: "TasksSegment"},
            customers: {type: SchemaTypes.ObjectId,
              ref: "TasksSegment"},
            earlyAdopters: {type: SchemaTypes.ObjectId,
              ref: "TasksSegment"},
            unfairAdvantage: {type: SchemaTypes.ObjectId,
              ref: "TasksSegment"},
            pathToCustomers: {type: SchemaTypes.ObjectId,
              ref: "TasksSegment"},
            uniqueValue: {type: SchemaTypes.ObjectId,
              ref: "TasksSegment"},
            simpleDescription: {type: SchemaTypes.ObjectId,
              ref: "TasksSegment"},
            priceModel: {type: SchemaTypes.ObjectId,
              ref: "TasksSegment"},
            costsStructure: {type: SchemaTypes.ObjectId,
              ref: "TasksSegment"}
          }
           /*{
            {name: "problem", data: TasksSegment},
            {name: "existingAlternatives", data: TasksSegment},
            {name: "solution", data: TasksSegment},
            {name: "indicators", data: TasksSegment},
            {name: "customers", data: TasksSegment},
            {name: "earlyAdopters", data: TasksSegment},
            {name: "unfairAdvantage", data: TasksSegment},
            {name: "pathToCustomers", data: TasksSegment},
            {name: "uniqueValue", data: TasksSegment},
            {name: "simpleDescription", data: TasksSegment},
            {name: "priceModel", data: TasksSegment},
            {name: "costsStructure", data: TasksSegment}
          }*/
          /* alternative
          segments: [[TasksSegment]], // list of columns of TasksSegments (possibly columns should be types of their own)
          structure: [] // somehow encode graphic structure
          */
        },
        { timestamps: true }
      )
      
    schema.method("toJSON", function() { // make id public; relevant for delete-able types
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
    
    const Project = mongoose.model("project", schema);
    return Project;
    };
