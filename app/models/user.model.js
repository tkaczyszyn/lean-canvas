const { SchemaTypes } = require("mongoose");
const db = require("../models/index.js");

module.exports = mongoose => {
  //const TasksSegment = mongoose.model("TasksSegment",schema);
    var schema = mongoose.Schema(
        {
          username: String,
          realName: String,
          // passwordHash: String, // TODO when authentication is implemented
          // hasPermissions: [], // TODO when permissions are implemented
          isInProjects: [{type:SchemaTypes.ObjectId, // TODO implement
            ref:"Project"}],
          id: Number,
        },
        { timestamps: true }
      )
      
    schema.method("toJSON", function() { //make id public
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
    
    const Project = mongoose.model("project", schema);
    return Project;
    };
