module.exports = app => {
    const users = require("../controllers/user.controller.js");
  
    var router = require("express").Router();
  
    // Create a new User
    router.post("/", user.create);
  
    // Retrieve all Users
    router.get("/", user.findAll);

    // Retrieve a single User by id
    router.get("/:id", user.findOne);
  
    // Change User data by id
    router.put("/:id", user.update);

    // Assign User to Project
    router.put("/assign_to_project/:userID:projectID", user.assignToProject); 

    // Retrieve list of projects user is part of
    router.get("/byProject/:projectID", user.listUsersInProject); // TODO remains to be implemented
    // TODO maybe would make sense to have as method in project
  
    // Delete a User by id
    router.delete("/:id", user.delete);
  
    // Delete all Users -- will require admin privileges
    router.delete("/", user.adminDeleteAll);
  
    app.use('/api/users', router);
  };