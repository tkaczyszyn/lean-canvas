module.exports = app => {
    const project = require("../controllers/project.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Project
    router.post("/", project.create);
  
    // Retrieve all Projects
    router.get("/", project.findAll);

    // Retrieve a single Project by id
    router.get("/:id", project.findOne);

    // Retrieve list of users in project
    // router.get("/hasUsers/:id", project.listUsersInProject); // TODO remains to be implemented
  
    // Change Project data by id
    router.put("/:id", project.update);
  
    // Delete a TasksSegment by id
    router.delete("/:id", project.delete);
  
    // Delete all Projects -- should require admin privileges
    router.delete("/", project.adminDeleteAll);
  
    app.use('/api/projects', router);
  };