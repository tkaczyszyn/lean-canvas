module.exports = app => {
    const tsegment = require("../controllers/tsegment.controller.js");
  
    var router = require("express").Router();
  
    // Create a new TasksSegment
    router.post("/", tsegment.create);
  
    // Retrieve all TasksSegments
    router.get("/", tsegment.findAll);
  
    // Retrieve a single TasksSegment by id
    router.get("/:id", tsegment.findOne);
  
    // Update a TasksSegment by id
    router.put("/:id", tsegment.update);
  
    // Delete a TasksSegment by id
    router.delete("/:id", tsegment.delete);
  
    // Delete all TasksSegments -- should require admin privileges
    router.delete("/", tsegment.adminDeleteAll);
  
    app.use('/api/segments', router);
  };