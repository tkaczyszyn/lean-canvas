const express = require("express");
const cors = require("cors");
const path = require("path");

//// PRELIMINARIES
const app = express();
var corsOptions = {
    origin: "http://localhost:8080"
}

app.use(cors(corsOptions))
// serve json
app.use(express.json());
// as application www form urlencoded
app.use(express.urlencoded({ extended: true }));
require("./app/routes/tsegment.routes.js")(app)
require("./app/routes/project.routes.js")(app)
require("./app/routes/user.routes.js")(app)



//// END PRELIMINARIES
app.use(express.static(path.join(__dirname, "..", "build"))); // serve react app
app.use(express.static("public")); // if not found, serve whatever from public folder
app.use((req, res, next) => { // if not found, serve 404
  res.sendFile(path.join(__dirname,"public", "404.html"));
});

const PORT = process.env.PORT || 8080;
app.listen(PORT,() => console.log(`Server listening at port ${PORT}`));

const db = require("./app/models/index.js");
console.log(db.url);
db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Connected to the db!");
  })
  .catch(err => {
    console.log("Cannot connect to the db!", err);
    
    process.exit();
  });
